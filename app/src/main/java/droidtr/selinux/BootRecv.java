package droidtr.selinux;

import android.content.*;
import android.widget.*;

public class BootRecv extends BroadcastReceiver{
	ZooperDB zdb;
    @Override
    public void onReceive(Context c, Intent intent) {
		try{
			zdb = new ZooperDB("settings",c.getPackageName());
			if(zdb.getValue("boot",false)){
				if(zdb.getValue("lchk",false))
					new MainActivity().execForStringOutput(1,"setenforce 1");
				else new MainActivity().execForStringOutput(1,"setenforce 0");
				Toast.makeText(c,"SELinux mode set successfully!",Toast.LENGTH_SHORT).show();
			} else zdb.removeValue("lchk");
		} catch(Exception e){
			return;
		}
    }
}
