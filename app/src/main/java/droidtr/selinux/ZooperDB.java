package droidtr.selinux;

/*
 This DB project is prepared
 by Furkan Karcıoğlu 
 25.08.2017 Fri
 */

import java.io.*;
import java.nio.charset.*;
import java.util.*;
import org.apache.commons.codec.binary.*;

public class ZooperDB {

	/*
	 Components:
	 1- Start
	 2- End
	 3- Split1 character
	 4- Split2 character
	 5- Split3 character
	 */

	private String [] components = {
		"<",">",";","•",","
	};

	private String fileName;
	private String fileFormat = "dtr";

	ZooperDB(String dbName, String packageName){
		String fn = "/data/data/"+packageName+"/zooper_prefs";
		File folder = new File(fn);
		if(!folder.exists())
			folder.mkdir();
		fileName = fn+"/"+dbName+"."+fileFormat;
		read();
	}

	public String getValue(String key, String def){
		String s = hm1.get(key);
		if(s != null && s != "")
			return decode(s);
		return def;
	}

	public int getValue(String key, int def){
		String s = hm1.get(key);
		if(s != null && s != "")
			return Integer.parseInt(decode(s));
		return def;
	}

	public float getValue(String key, float def){
		String s = hm1.get(key);
		if(s != null && s != "")
			return Float.parseFloat(decode(s));
		return def;
	}

	public double getValue(String key, double def){
		String s = hm1.get(key);
		if(s != null && s != "")
			return Double.parseDouble(decode(s));
		return def;
	}

	public boolean getValue(String key, boolean def){
		String s = hm1.get(key);
		if(s != null && s != "")
			return Boolean.parseBoolean(decode(s));
		return def;
	}

	public void setValue(String key, String value){
		value = encode(value);
		hm1.put(key,value);
		if(!(sb1.toString().contains(key+components[4])))
			sb1.append(key+components[4]);
		refresh();
	}

	public void setValue(String key, int value){
		String val = encode(value+"");
		hm1.put(key,val);
		if(!(sb1.toString().contains(key+components[4])))
			sb1.append(key+components[4]);
		refresh();
	}

	public void setValue(String key, float value){
		String val = encode(value+"");
		hm1.put(key,val);
		if(!(sb1.toString().contains(key+components[4])))
			sb1.append(key+components[4]);
		refresh();
	}

	public void setValue(String key, double value){
		String val = encode(value+"");
		hm1.put(key,val);
		if(!(sb1.toString().contains(key+components[4])))
			sb1.append(key+components[4]);
		refresh();
	}

	public void setValue(String key, boolean value){
		String val = encode(value+"");
		hm1.put(key,val);
		if(!(sb1.toString().contains(key+components[4])))
			sb1.append(key+components[4]);
		refresh();
	}

	public void removeValue(String key){
		hm1.remove(key);
		String t = sb1.toString().replace(key+components[4],"");
		sb1 = new StringBuilder();
		sb1.append(t);
		refresh();
	}
	
	public void removeDB(){
		hm1.clear();
		sb1 = new StringBuilder();
		new File(fileName).delete();
	}

	private void refresh(){
		write();
		read();
	}

	private void write(){
		StringBuilder wr = new StringBuilder();
		String[] key = sb1.toString().split(components[4]);
		for(int i = 0;i != key.length;i++)
			wr.append(components[0]+key[i]+components[3]+hm1.get(key[i])+components[1]+components[2]);
		wr.replace(wr.length()-1,wr.length(),"");
		if(!(wr.toString().equals(components[0]+components[3]+"null"+components[1]))){
			try{
				File f = new File(fileName);
				OutputStream os = new FileOutputStream(f);
				for(byte x : wr.toString().getBytes())
					os.write(x);
				os.close();
			} catch(Exception e){}
		}
		
	}

	private void read(){
		StringBuilder rd = new StringBuilder();
		sb1 = new StringBuilder();
		try{
			File f = new File(fileName);
			Scanner sc = new Scanner(f);
			while(sc.hasNext())
				rd.append(sc.next());
			String[] pv = rd.toString().replaceAll(components[0],"")
				.replaceAll(components[1],"").split(components[2]);
			for(int i = 0;i != pv.length;i++){
				String[] xx = pv[i].split(components[3]);
				hm1.put(xx[0],xx[1]);
				sb1.append(xx[0]+components[4]);
			}
		} catch(Exception e){}
	}

	private String encode(String s){
		String enc = new String(Base64.encodeBase64(s.getBytes()),StandardCharsets.UTF_8);
		String[] en = enc.split("");
		StringBuilder sb1 = new StringBuilder();
		for(int i = 0;i != en.length;i++)
			sb1.append(en[i]+randomChar());
		return sb1.toString();
	}

	private String decode(String s){
		for(int i = 0;i != enc_dec_str.length;i++)
			s = s.replaceAll(enc_dec_str[i],"");
		return new String(Base64.decodeBase64(s.getBytes()),StandardCharsets.UTF_8);
	}

	private String[] enc_dec_str = {
		"∆", "¶", "℅", "÷", "°", "©",
		"®", "™", "π", "¥", "£", "$",
		"¢", "×", "√", "/", "`"
	};

	private String randomChar(){
		String t = "";
		for(int i = 0;i < new Random().nextInt(5);i++)
			t += enc_dec_str[new Random().nextInt(enc_dec_str.length)];
		return t;
	}

	private StringBuilder sb1 = new StringBuilder();
	private HashMap<String,String> hm1 = new HashMap<String,String>();

}
