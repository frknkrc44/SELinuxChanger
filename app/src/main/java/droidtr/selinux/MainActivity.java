package droidtr.selinux;

import android.app.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.widget.*;
import java.io.*;

public class MainActivity extends Activity {
	
	Switch s1;
	Switch s2;
	ZooperDB zdb;
	Toast t;
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		s1 = (Switch) findViewById(R.id.selinux);
		s2 = (Switch) findViewById(R.id.boot);
		s1.setChecked(getSELinux());
		t = Toast.makeText(this,"",Toast.LENGTH_SHORT);
		zdb = new ZooperDB("settings",getPackageName());
		s2.setChecked(zdb.getValue("boot",false));
		
		s1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton p1, boolean p2){
				setSELinux(p2);
				zdb.setValue("lchk",p2);
			}
		});
		
		s2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton p1, boolean p2){
				zdb.setValue("boot",p2);
				if(p2) showToast("On Boot mode enabled!");
				else showToast("On Boot mode disabled!");
			}
		});
    }
	
	boolean getSELinux(){
		try {
			String enf = execForStringOutput(1,"getenforce");
			if(enf.contains("Enf"))
				return true;
			return false;
		} catch(Exception e){
			return false;
		} catch(Error er){
			return false;
		}
	}
	
	void setSELinux(boolean seLinux){
		int i = 0;
		if(seLinux) i = 1;
		try {
			execForStringOutput(1,"setenforce "+i);
			if(seLinux) showToast("SELinux Enabled");
			else showToast("SELinux Disabled");
		} catch(Exception e){} catch(Error er){}
	}
	
	String execForStringOutput(int shellType,String command) throws IOException {
		String st = new String();
		if (shellType == 0) st = "sh";
		if (shellType == 1) st = "su";
		if (shellType == 2) st = "bash";
		if (shellType > 2 || shellType < 0)
			return null;
		return execForStringOutput(st,command);
	}

	String execForStringOutput(String customShell,String command) throws IOException {
		if (customShell != "" && customShell != null && command != "" && command != null) {
			String line;
			StringBuilder s = new StringBuilder();
			java.lang.Process p = Runtime.getRuntime().exec(customShell);
			DataOutputStream dos = new DataOutputStream(p.getOutputStream());
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			dos.writeBytes(command+"\n");
			dos.flush();
			dos.close();
			while ((line = input.readLine()) != null)
				s.append(line);
			input.close();
			return s.toString();
		} else return null;
	}
	
	void showToast(String text){
		t.setText(text);
		t.show();
	}
	
}
